import sqlite3
import sys
conn = sqlite3.connect("mydb.sqlite3")
cursor = conn.cursor()

table_name = "user_credentials"

sql_create = "create table if not exists " + table_name + "(username text primary key, password text)"
cursor.execute(sql_create)

## create/update a random record
username = "abc"
import random
import hashlib
username = "abc"
password = hashlib.md5(str(random.random())).hexdigest()

query = "INSERT OR IGNORE INTO " + table_name + " VALUES(?,?) "
cursor.execute(query, (username, password))

query = "UPDATE " + table_name + " SET password = '" + password + "' WHERE username = '" + username + "'"
cursor.execute(query)

print "added", username, password
conn.commit()





